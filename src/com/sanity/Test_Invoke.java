package com.sanity;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;



public class Test_Invoke {


	@Test
	public static void Login () throws InterruptedException{
		String appUrl = "https://underservedcare.com/users/sign_in" ;
		System.out.println("patient booking started");
		WebDriver driver = new FirefoxDriver();
		driver.get(appUrl); // launch the Firefox browser and open the application url
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		//declare and initialize the variable to store the expected title of the webpage.
		Thread.sleep(10000);
		System.out.println("................................. "); 

		//enter Valid Email
		WebElement username = driver.findElement(By.id("user_email"));
		username.clear();
		username.sendKeys("pavankishann+122@gmail.com");
		//enter Valid password
		WebElement password = driver.findElement(By.id("user_password"));
		password.clear();
		password.sendKeys("Test@12345");
		//click on login button
		WebElement LogInButton = driver.findElement(By.xpath("//*[text()='Login']"));
		LogInButton.click();
		//close web browser
		//driver.close();

		System.out.println(driver.getTitle()); 
		String s= driver.getCurrentUrl();
		System.out.println(s);

		System.out.println("Test script executed successfully.");
		// System.exit(0);//terminate program

		Thread.sleep(5000);
		String expectedTitle = "Ucareconnect";
		//fetch the title of the web page and save it into a string variable
		String actualTitle = driver.getTitle();
		System.out.println("The actual Text from the browser is ......: " + actualTitle); 


		//comparing with expected and actual title
		if (expectedTitle.equals(actualTitle)) {
			System.out.println("Verification Successful - The correct title is displayed on the web page");
		}
		else {
			System.out.println("Verification Failed - An incorrect title is displayed on the web page");
		}
		driver.findElement(By.xpath("//*[text()='Book an appointment']")).click();
		System.out.println("book appointment button is clicked");
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//*[@class=\"table-user-list\"])[1]")).click();
		System.out.println("provider is selected");
		try{  
			driver.findElement(By.xpath("//*[text()='Create appointment']")).click();  
		}
		catch(Exception e)
		{
			System.out.println("There is an existing Appointment Hence Leaving the waiting room and Re-booking the appointment");

			driver.findElement(By.xpath("//*[text()='Join Back Waiting Room ']")).click(); 
			WebDriverWait wait=new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@value='Leave Waiting Room']")));
			driver.findElement(By.xpath("//*[@value='Leave Waiting Room']")).click();
			driver.findElement(By.xpath("//*[text()='Book an appointment']")).click();
			System.out.println("book appointment button is clicked");
			Thread.sleep(5000);
			driver.findElement(By.xpath("(//*[@class=\"table-user-list\"])[3]")).click();
			System.out.println("provider is selected");
			driver.findElement(By.xpath("//*[text()='Create appointment']")).click();  

		} 
		// driver.findElement(By.xpath("//*[text()='Create appointment']")).click();
		System.out.println("create appointment button is clicked");
		driver.findElement(By.xpath("//*[@id='appointment_payment_plan_id']")).click();
		System.out.println("dropdown is clicked");
		driver.findElement(By.xpath("//*[@id='appointment_payment_plan_id']/option[1]")).click();
		System.out.println("first option is the dropdown is selected");
		driver.findElement(By.xpath("//*[@id='appointment_description']")).sendKeys("I am sick");
		System.out.println("description is entered");
		driver.findElement(By.xpath("//*[@id='appointment_payment_acceptance']")).click();
		System.out.println("accept terms and conditions");
		driver.findElement(By.xpath("//*[@id='request_appointment_button']")).click();
		System.out.println("click create appointment button");
		if (driver.findElement(By.xpath("//*[@id='flash_notice']")).isDisplayed())
		{
			System.out.println("appointment booking is sucessfull and pass");	
		}else {
			System.out.println("appointment booking is Unsucessfull");	
		}
	}


	@Test
	public static void Provider_login () throws InterruptedException{
		String appUrl = "https://rgroup.underservedcare.com/users/sign_in" ;
		System.out.println("provider booking started");
		WebDriver driver = new FirefoxDriver();
		driver.get(appUrl); // launch the Firefox browser and open the application url
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		//declare and initialize the variable to store the expected title of the webpage.
		Thread.sleep(10000);
		System.out.println("................................. "); 


		WebElement username = driver.findElement(By.id("user_email"));
		username.clear();
		username.sendKeys("pavankishann+123@gmail.com");

		WebElement password = driver.findElement(By.id("user_password"));
		password.clear();
		password.sendKeys("Test@12345");

		WebElement LogInButton = driver.findElement(By.xpath("//*[text()='Login']"));
		LogInButton.click();


		System.out.println(driver.getTitle()); 
		System.out.println("Test script executed successfully.");

		Thread.sleep(5000);
		String expectedTitle = "Ucareconnect";

		String actualTitle = driver.getTitle();
		System.out.println("The actual Text from the browser is ......: " + actualTitle); 
		System.out.println("booking appointment started");

		//comparing with expected and actual title
		if (expectedTitle.equals(actualTitle)) {
			System.out.println("Verification Successful - The correct title is displayed on the web page");
		}
		else {
			System.out.println("Verification Failed - An incorrect title is displayed on the web page");
		}
		driver.findElement(By.xpath("//*[text()='Book Appointment']")).click();
		System.out.println("book appointment tab is clicked");
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//*[text()='Book '])[1]")).click();
		System.out.println("patient is selected frmom list");
		//driver.findElement(By.xpath("//*[text()='Quick']")).click();
		
		driver.findElement(By.xpath("//*[@id='appointment_payment_plan_id']")).click();
		System.out.println("dropdown is clicked");
		driver.findElement(By.xpath("//*[@id='appointment_payment_plan_id']/option[1]")).click();
		System.out.println("first option is the dropdown is selected");
		driver.findElement(By.xpath("//*[@id='appointment_description']")).sendKeys("I am sick");
		System.out.println("description is entered");

		driver.findElement(By.xpath("//*[@id='request_appointment_button']")).click();
		System.out.println("click create appointment button");
		if (driver.findElement(By.xpath("//*[@id='flash_notice']")).isDisplayed())
		{
			System.out.println("appointment booking is sucessfull form provider and pass");	
		}else {
			System.out.println("appointment booking is Unsucessfull from provider");	
		}
	}

	@Test
	public static void video_consultation () throws InterruptedException{
		String appUrl = "https://underservedcare.com/users/sign_in" ;
		System.out.println("videos consultaion started");
		WebDriver driver = new FirefoxDriver();
		driver.get(appUrl); 
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);

		Thread.sleep(10000);
		System.out.println("................................. "); 
		WebElement username = driver.findElement(By.id("user_email"));
		username.clear();
		username.sendKeys("pavankishann+122@gmail.com");

		WebElement password = driver.findElement(By.id("user_password"));
		password.clear();
		password.sendKeys("Test@12345");

		WebElement LogInButton = driver.findElement(By.xpath("//*[text()='Login']"));
		LogInButton.click();

		System.out.println(driver.getTitle()); 

		System.out.println("Test script executed successfully.");
		Thread.sleep(5000);
		String expectedTitle = "Ucareconnect";

		String actualTitle = driver.getTitle();
		System.out.println("The actual Text from the browser is ......: " + actualTitle); 
		if (expectedTitle.equals(actualTitle)) {
			System.out.println("Verification Successful - The correct title is displayed on the web page");
		}
		else {
			System.out.println("Verification Failed - An incorrect title is displayed on the web page");
		}
		driver.findElement(By.xpath("//*[text()='Book an appointment']")).click();
		System.out.println("book appointment button is clicked");
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//*[@class='table-user-list'])[3]")).click();
		System.out.println("provider is selected");
		try{  
			driver.findElement(By.xpath("//*[text()='Create appointment']")).click();  
		}
		catch(Exception e)
		{
			System.out.println("There is an existing Appointment Hence Leaving the waiting room and Re-booking the appointment");

			driver.findElement(By.xpath("//*[text()='Join Back Waiting Room ']")).click(); 
			WebDriverWait wait=new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@value='Leave Waiting Room']")));
			driver.findElement(By.xpath("//*[@value='Leave Waiting Room']")).click();
			driver.findElement(By.xpath("//*[text()='Book an appointment']")).click();
			System.out.println("book appointment button is clicked");
			Thread.sleep(5000);
			driver.findElement(By.xpath("(//*[@class=\"table-user-list\"])[3]")).click();
			System.out.println("provider is selected");
			driver.findElement(By.xpath("//*[text()='Create appointment']")).click();  

		} 
		// driver.findElement(By.xpath("//*[text()='Create appointment']")).click();
		System.out.println("create appointment button is clicked");
		driver.findElement(By.xpath("//*[@id='appointment_payment_plan_id']")).click();
		System.out.println("dropdown is clicked");
		driver.findElement(By.xpath("//*[@id='appointment_payment_plan_id']/option[1]")).click();
		System.out.println("first option is the dropdown is selected");
		driver.findElement(By.xpath("//*[@id='appointment_description']")).sendKeys("I am sick");
		System.out.println("description is entered");
		driver.findElement(By.xpath("//*[@id='appointment_payment_acceptance']")).click();
		System.out.println("accept terms and conditions");
		driver.findElement(By.xpath("//*[@id='request_appointment_button']")).click();
		System.out.println("click create appointment button");
		if (driver.findElement(By.xpath("//*[@id='flash_notice']")).isDisplayed())
		{
			System.out.println("appointment booking is sucessfull");	
		}else {
			System.out.println("appointment booking is Unsucessfull");	
		}
		driver.findElement(By.xpath("//*[@class=\"down_arrow\"]")).click();
		System.out.println("clicking on drown arrow to logout");
		driver.findElement(By.xpath("//*[@class='icon-off']")).click();
		System.out.println("clicking on logout text to logout");



		 WebDriver driver1 = new FirefoxDriver();
		  driver1.get(appUrl); // launch the Firefox browser and open the application url
		  driver1.manage().window().maximize();

		WebElement username1 = driver.findElement(By.id("user_email"));
		username1.clear();
		username1.sendKeys("pavankishann+123@gmail.com");

		WebElement password1 = driver.findElement(By.id("user_password"));
		password1.clear();
		password1.sendKeys("Test@12345");

		WebElement LogInButton1 = driver.findElement(By.xpath("//*[text()='Login']"));
		LogInButton1.click();
		String s= driver.getCurrentUrl();
		System.out.println(s);

		driver.findElement(By.xpath("//img [@src='/assets/play_active_icon.png']")).click();
		System.out.println("click to accept video session");
		Alert alert = driver.switchTo().alert();
		alert.accept(); 
		driver.findElement(By.xpath(".//*[@id='section-upgrade-install']/button")).click();


		if (driver.findElement(By.xpath("//img [@src='/assets/icons/stop.png']")).isDisplayed()) {
			driver.findElement(By.xpath("//img [@src='/assets/icons/stop.png']")).click();

			System.out.println("video concultation sucessfull");	
		}else {
			System.out.println(" Unsucessfull");	
		} 
	}


	@Test
	public static void Waiting_Room () throws InterruptedException{
		String appUrl = "https://underservedcare.com/users/sign_in" ;
		System.out.println("waiting room test started");
		WebDriver driver = new FirefoxDriver();
		driver.get(appUrl); // launch the Firefox browser and open the application url
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);

		WebElement username = driver.findElement(By.id("user_email"));
		username.clear();
		username.sendKeys("pavankishann+122@gmail.com");

		WebElement password = driver.findElement(By.id("user_password"));
		password.clear();
		password.sendKeys("Test@12345");

		WebElement LogInButton = driver.findElement(By.xpath("//*[text()='Login']"));
		LogInButton.click();

		System.out.println(driver.getTitle()); 
		System.out.println("Test script executed successfully.");
		// System.exit(0);//terminate program

		Thread.sleep(5000);
		String expectedTitle = "Ucareconnect";
		//fetch the title of the web page and save it into a string variable
		String actualTitle = driver.getTitle();
		System.out.println("The actual Text from the browser is ......: " + actualTitle); 
		//comparing with expected and actual title
		if (expectedTitle.equals(actualTitle)) {
			System.out.println("Verification Successful - The correct title is displayed on the web page");
		}
		else {
			System.out.println("Verification Failed - An incorrect title is displayed on the web page");
		}
		driver.findElement(By.xpath("//*[text()='Book an appointment']")).click();
		System.out.println("book appointment button is clicked");
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//*[@class=\"table-user-list\"])[3]")).click();
		System.out.println("provider is selected");
		try{  
			driver.findElement(By.xpath("//*[text()='Create appointment']")).click();  
		}
		catch(Exception e)
		{
			System.out.println("There is an existing Appointment Hence Leaving the waiting room and Re-booking the appointment");

			driver.findElement(By.xpath("//*[text()='Join Back Waiting Room ']")).click(); 
			WebDriverWait wait=new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@value='Leave Waiting Room']")));
			driver.findElement(By.xpath("//*[@value='Leave Waiting Room']")).click();
			driver.findElement(By.xpath("//*[text()='Book an appointment']")).click();
			System.out.println("book appointment button is clicked");
			Thread.sleep(5000);
			driver.findElement(By.xpath("(//*[@class=\"table-user-list\"])[3]")).click();
			System.out.println("provider is selected");
			driver.findElement(By.xpath("//*[text()='Create appointment']")).click();  

		} 
		//  driver.findElement(By.xpath("//*[text()='Create appointment']")).click();


		System.out.println("create appointment button is clicked");



		driver.findElement(By.xpath("//*[@id='appointment_payment_plan_id']")).click();
		System.out.println("dropdown is clicked");
		driver.findElement(By.xpath("//*[@id='appointment_payment_plan_id']/option[1]")).click();
		System.out.println("first option is the dropdown is selected");
		driver.findElement(By.xpath("//*[@id='appointment_description']")).sendKeys("I am patient");
		System.out.println("description is entered");
		driver.findElement(By.xpath("//*[@id='appointment_payment_acceptance']")).click();
		System.out.println("accept terms and conditions");
		driver.findElement(By.xpath("//*[@id='request_appointment_button']")).click();
		System.out.println("click create appointment button");
		if (driver.findElement(By.xpath("//*[@id='flash_notice']")).isDisplayed())
		{
			System.out.println("appointment booking is sucessfull");	
		}else {
			System.out.println("appointment booking is Unsucessfull");	
		}
		driver.findElement(By.xpath("//*[@class=\"down_arrow\"]")).click();
		System.out.println("clicking on drown arrow to logout");
		driver.findElement(By.xpath("//*[@class='icon-off']")).click();
		System.out.println("clicking on logout text to logout");

		WebDriver driver1 = new FirefoxDriver();
		driver1.get(appUrl); // launch the Firefox browser and open the application url
		driver1.manage().window().maximize();

		WebElement username1 = driver.findElement(By.id("user_email"));
		username1.clear();
		username1.sendKeys("pavankishann+123@gmail.com");

		WebElement password1 = driver.findElement(By.id("user_password"));
		password1.clear();	
		password1.sendKeys("Test@12345");

		WebElement LogInButton1 = driver.findElement(By.xpath("//*[text()='Login']"));
		System.out.println("login sucessfull");
		LogInButton1.click();
		System.out.println("l//////////////////////////");
		WebDriverWait wait=new WebDriverWait(driver, 10);
		System.out.println("------------------------------");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@*='waiting_room']")));
		WebElement waitingRoomTab = driver.findElement(By.xpath("//*[@*='waiting_room']"));
		waitingRoomTab.click();
		if (driver.findElement(By.xpath("//*[text()='Quick Appointment']")).isDisplayed())

		{
			System.out.println("patient is on waiting room pass");	
		}else {
			System.out.println("error failure");	
		}
	}







	@Test
	public static void message_checking () throws InterruptedException{
		String appUrl = "https://underservedcare.com/users/sign_in" ;
		System.out.println("message checking started");
		WebDriver driver = new FirefoxDriver();
		driver.get(appUrl); // launch the Firefox browser and open the application url
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);

		WebElement username = driver.findElement(By.id("user_email"));
		username.clear();
		username.sendKeys("pavankishann+122@gmail.com");

		WebElement password = driver.findElement(By.id("user_password"));
		password.clear();
		password.sendKeys("Test@12345");

		WebElement LogInButton = driver.findElement(By.xpath("//*[text()='Login']"));
		LogInButton.click();

		System.out.println(driver.getTitle()); 
		System.out.println("Test script executed successfully.");
		// System.exit(0);//terminate program

		Thread.sleep(5000);
		String expectedTitle = "Ucareconnect";
		//fetch the title of the web page and save it into a string variable
		String actualTitle = driver.getTitle();
		System.out.println("The actual Text from the browser is ......: " + actualTitle); 
		//comparing with expected and actual title
		if (expectedTitle.equals(actualTitle)) {
			System.out.println("Verification Successful - The correct title is displayed on the web page");
		}
		else {
			System.out.println("Verification Failed - An incorrect title is displayed on the web page");
		}
		driver.findElement(By.xpath("//*[text()=' Messages ']")).click();
		System.out.println("click on messages tab");
		driver.findElement(By.xpath("(//*[@class='itemdiv commentdiv'])[1]")).click();
		System.out.println("click on 1st provider");
		driver.findElement(By.xpath("//*[@id='chat_room_content']")).sendKeys("hello bosss");
		driver.findElement(By.xpath("//*[@id='chat_submit']")).click();
		driver.findElement(By.xpath("//*[@class=\"down_arrow\"]")).click();
		System.out.println("clicking on drown arrow to logout");
		driver.findElement(By.xpath("//*[@class='icon-off']")).click();
		System.out.println("clicking on logout text to logout");


		WebDriver driver1 = new FirefoxDriver();
		driver1.get(appUrl); // launch the Firefox browser and open the application url
		driver1.manage().window().maximize();

		WebElement username1 = driver.findElement(By.id("user_email"));
		username1.clear();
		username1.sendKeys("pavankishann+123@gmail.com");

		WebElement password1 = driver.findElement(By.id("user_password"));
		password1.clear();	
		password1.sendKeys("Test@12345");

		WebElement LogInButton1 = driver.findElement(By.xpath("//*[text()='Login']"));
		System.out.println("login sucessfull");
		LogInButton1.click();
		System.out.println("login sucessfull");
		String s= driver.getCurrentUrl();
		System.out.println(s);
		driver.findElement(By.xpath("//*[text()=' Messages ']")).click();
		driver.findElement(By.xpath("//*[@*='name_158']")).click();
		if (driver.findElement(By.xpath(".//*[@id='chat_159158']/div[21]/div[2]/div")).isDisplayed())
		{
			System.out.println("message is displayed and pass");
		}
		else
		{	
			System.out.println("message isnot  displayeed");
		}

		driver.quit();
	}




	@Test
	public static void deactiv_Provider () throws InterruptedException{
		String appUrl = "https://underservedcare.com/users/sign_in" ;
		System.out.println("test acse to lock provider started");
		WebDriver driver = new FirefoxDriver();
		driver.get(appUrl); 
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);


		System.out.println("................................. "); 
		WebElement username = driver.findElement(By.id("user_email"));
		username.clear();
		username.sendKeys("rkriteshkumar4u@gmail.com");

		WebElement password = driver.findElement(By.id("user_password"));
		password.clear();
		password.sendKeys("Rajesh@1234");

		WebElement LogInButton = driver.findElement(By.xpath("//*[text()='Login']"));
		LogInButton.click();

		System.out.println(driver.getTitle()); 

		System.out.println("Test script executed successfully.");
		Thread.sleep(5000);
		String expectedTitle = "Ucareconnect";

		String actualTitle = driver.getTitle();
		System.out.println("The actual Text from the browser is ......: " + actualTitle); 
		if (expectedTitle.equals(actualTitle)) {
			System.out.println("Verification Successful - The correct title is displayed on the web page");
		}
		else {
			System.out.println("Verification Failed - An incorrect title is displayed on the web page");
		}
		driver.findElement(By.xpath(".//*[@id='appointments-table_filter']/label/input[1] ")).sendKeys("bhuvna provider");
		driver.findElement(By.xpath("//img [@src='/assets/icons/unlocked.png']")).click();
		Alert alert = driver.switchTo().alert();
		alert.accept(); 
		driver.findElement(By.xpath("//*[@class=\"down_arrow\"]")).click();
		System.out.println("clicking on drown arrow to logout");
		driver.findElement(By.xpath("//*[@class='icon-off']")).click();
		System.out.println("clicking on logout text to logout");

		WebElement username1 = driver.findElement(By.id("user_email"));
		username1.clear();
		username1.sendKeys("pavankishann+123@gmail.com");

		WebElement password1 = driver.findElement(By.id("user_password"));
		password1.clear();
		password1.sendKeys("Test@12345");

		WebElement LogInButton1 = driver.findElement(By.xpath("//*[text()='Login']"));
		LogInButton1.click();
		if (driver.findElement(By.xpath("//*[text()='Account is deactivated, please contact administrator.']")).isDisplayed())
		{
			System.out.println("providers account is locked and test case is pass");

		}
		else
		{	
			System.out.println("providers account is not locked and test cases is fail");
		}
	




		String appUrl1 = "https://underservedcare.com/users/sign_in" ;
		System.out.println("test case to unlock provider account started");
		//WebDriver driver = new FirefoxDriver();
		
		
		driver.navigate().to(appUrl1); 
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);

		Thread.sleep(10000);
		System.out.println("................................. "); 
		WebElement username_1 = driver.findElement(By.id("user_email"));
		username_1.clear();
		username_1.sendKeys("rkriteshkumar4u@gmail.com");

		WebElement password12 = driver.findElement(By.id("user_password"));
		password12.clear();
		password12.sendKeys("Rajesh@1234");

		WebElement LogInButton_1 = driver.findElement(By.xpath("//*[text()='Login']"));
		LogInButton_1.click();

		System.out.println(driver.getTitle()); 

		System.out.println("Test script executed successfully.");
		Thread.sleep(5000);
		String expectedTitle_1 = "Ucareconnect";

		String actualTitle_1 = driver.getTitle();
		System.out.println("The actual Text from the browser is ......: " + actualTitle_1); 
		if (expectedTitle_1.equals(actualTitle_1)) {
			System.out.println("Verification Successful - The correct title is displayed on the web page");
		}
		else {
			System.out.println("Verification Failed - An incorrect title is displayed on the web page");
		}
		driver.findElement(By.xpath(".//*[@id='appointments-table_filter']/label/input[1] ")).sendKeys("bhuvna provider");
		driver.findElement(By.xpath("//img [@src='/assets/icons/locked.png']")).click();
		Alert alert1 = driver.switchTo().alert();
		alert1.accept(); 
		driver.findElement(By.xpath("//*[@class=\"down_arrow\"]")).click();
		System.out.println("clicking on drown arrow to logout");
		driver.findElement(By.xpath("//*[@class='icon-off']")).click();
		System.out.println("clicking on logout text to logout");

		WebElement username_11 = driver.findElement(By.id("user_email"));
		username_11.clear();
		username_11.sendKeys("pavankishann+123@gmail.com");

		WebElement password_12 = driver.findElement(By.id("user_password"));
		password_12.clear();
		password_12.sendKeys("Test@12345");

		WebElement LogInButton_12 = driver.findElement(By.xpath("//*[text()='Login']"));
		LogInButton_12.click();
		if (driver.findElement(By.xpath("//*[@class='header_name']")).isDisplayed())
		{
			System.out.println("providers account is activated and test case is pass");

		}
		else
		{	
			System.out.println("providers account is inactive and test cases is fail");
		}


	}	
}





